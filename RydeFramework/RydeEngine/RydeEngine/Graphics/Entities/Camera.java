package RydeEngine.Graphics.Entities;

import org.lwjgl.util.vector.Vector3f;

public class Camera {
	private Vector3f rotation = new Vector3f(0,0,0);
	private Vector3f position = new Vector3f(0,0,0);

	public Vector3f getRotation() {
		return rotation;
	}

	public Vector3f getPosition() {
		return position;
	}

}
