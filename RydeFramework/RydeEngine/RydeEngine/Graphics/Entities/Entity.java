package RydeEngine.Graphics.Entities;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import RydeEngine.Graphics.Models.Model;
import RydeEngine.Graphics.Textures.Texture;
import RydeEngine.System.ObjectDatabase;

public class Entity {
	private String name;
	private Vector3f scale;
	private Vector3f position;
	private Vector3f rotation;
	
	private Model model;
	private Texture texture;
	
	private ObjectDatabase related;
	
	private Vector3f reference;
	
	private boolean enabled = true;
	
	public Entity(String name, Model model, Texture texture){
		this.name = name;
		this.model = model;
		this.texture = texture;
		
		scale = new Vector3f(1.0f,1.0f,1.0f);
		position = new Vector3f(0.0f,0.0f,0.0f);
		rotation = new Vector3f(0.0f,0.0f,0.0f);
		
		buildRelated();
		
		setReference(position);
	}
	
	public Entity(String name, Model model, Texture texture, float positionX, float positionY, float positionZ){
		this.name = name;
		this.model = model;
		this.texture = texture;
		
		scale = new Vector3f(1.0f,1.0f,1.0f);
		position = new Vector3f(positionX,positionY,positionZ);
		rotation = new Vector3f(0.0f,0.0f,0.0f);
		
		buildRelated();
		
		setReference(position);
	}
	
	public Entity(String name, Model model, Texture texture, float positionX, float positionY, float positionZ, float scale){
		this.name = name;
		this.model = model;
		this.texture = texture;
		
		this.scale = new Vector3f(scale,scale,scale);
		this.position = new Vector3f(positionX,positionY,positionZ);
		this.rotation = new Vector3f(0.0f,0.0f,0.0f);
		
		buildRelated();
		
		setReference(position);
	}
	
	public Entity(String name, Model model, Texture texture, float positionX, float positionY, float positionZ, float rotX, float rotY, float rotZ){
		this.name = name;
		this.model = model;
		this.texture = texture;
		
		this.scale = new Vector3f(1.0f,1.0f,1.0f);
		this.position = new Vector3f(positionX,positionY,positionZ);
		this.rotation = new Vector3f(rotX, rotY, rotZ);
		
		buildRelated();
		
		setReference(position);
	}
	
	public Entity(String name, Model model, Texture texture, float positionX, float positionY, float positionZ, float rotX, float rotY, float rotZ, float scale){
		this.name = name;
		this.model = model;
		this.texture = texture;
		
		this.scale = new Vector3f(scale,scale,scale);
		this.position = new Vector3f(positionX,positionY,positionZ);
		this.rotation = new Vector3f(rotX, rotY, rotZ);
		
		buildRelated();
		
		setReference(position);
	}
	
	public Entity(String name, Model model, Texture texture, float positionX, float positionY, float positionZ, float rotX, float rotY, float rotZ, float scaleX, float scaleY, float scaleZ){
		this.name = name;
		this.model = model;
		this.texture = texture;
		
		this.scale = new Vector3f(scaleX,scaleY,scaleZ);
		this.position = new Vector3f(positionX,positionY,positionZ);
		this.rotation = new Vector3f(rotX, rotY, rotZ);
		
		buildRelated();
		
		setReference(position);
	}
	
	private void buildRelated(){
		this.related = new ObjectDatabase();
		
		this.related.addTable("Children");
		this.related.addTable("Parents");
	}
	
	public void setReference(Vector3f reference){
		this.reference = reference;
		this.reference.y += model.getHeightMod();
	}
	
	public void move(float directionX, float directionY, float directionZ){
		this.position.x += directionX;
		this.position.y += directionY;
		this.position.z += directionZ;
	}
	
	public void rotate(float directionX, float directionY, float directionZ){
		this.rotation.x += directionX;
		this.rotation.y += directionY;
		this.rotation.z += directionZ;
	}
	
	public void scale(float directionX, float directionY, float directionZ){
		this.scale.x += directionX;
		this.scale.y += directionY;
		this.scale.z += directionZ;
	}
	
	public void setName(String text){
		name = text;
	}

	public String getName() {
		return name;
	}

	public Vector3f getScale() {
		return scale;
	}

	public Vector3f getPosition() {
		return position;
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public Model getModel() {
		return model;
	}

	public Texture getTexture() {
		return texture;
	}

	public ObjectDatabase getRelated() {
		return related;
	}

	public Vector3f getReference() {
		return reference;
	}
	
	public boolean isEnabled(){
		return enabled;
	}

	public Vector2f getTextureOffset() {
		return texture.getOffset();
	}
}
