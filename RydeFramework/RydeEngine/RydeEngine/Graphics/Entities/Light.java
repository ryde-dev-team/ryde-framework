package RydeEngine.Graphics.Entities;

import org.lwjgl.util.vector.Vector3f;

public class Light{
	private Vector3f position;
	private Vector3f color;

	public Vector3f getPosition() {
		return position;
	}
	
	public Vector3f getColor() {
		return color;
	}

}
