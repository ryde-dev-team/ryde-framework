package RydeEngine.Graphics.Entities;

import RydeEngine.Graphics.Graphics;
import RydeEngine.Graphics.Models.Model;
import RydeEngine.Graphics.Textures.Texture;

public class Sphere extends Entity{
	
	public Sphere(){
		super("Sphere", Graphics.getModel("sphere"), Graphics.getTexture("test"));
	}
}
