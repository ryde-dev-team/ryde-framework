package RydeEngine.Graphics;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import RydeEngine.Graphics.Entities.Camera;
import RydeEngine.Graphics.Entities.Entity;
import RydeEngine.Graphics.Models.Model;
import RydeEngine.Graphics.Renderers.Renderer;
import RydeEngine.Graphics.Textures.Texture;
import RydeEngine.System.ObjectDatabase;
import RydeEngine.System.ObjectEntry;
import RydeEngine.System.ObjectList;
import RydeEngine.System.Timer;
import RydeEngine.System.Window;
import RydeEngine.System.Loaders.Loader;

public class Graphics {
	private static boolean active = false;
	
	private static Window window;
	private static Timer renderTimer;
	
	private static ObjectDatabase renderDb = new ObjectDatabase();
	
	public static int MAX_MODELS = 999;
	public static int MAX_TEXTURES = 999;
	
	private static Camera activeCamera = new Camera();
	
	public static void start(int width, int height, int fps){
		window = new Window(width, height, fps);
		renderTimer = new Timer(1000/fps);
		Renderer.start(window);
		setActive(true);
		renderTimer.start();
		
		renderDb.addTable("Textures");
		renderDb.addTable("Models");
		renderDb.addTable("Entities");
		renderDb.addTable("Terrains");
		renderDb.addTable("Lights");
	}
	
	public static void setActive(boolean state){
		active = state;
	}
	
	public static boolean isActive(){
		return active;
	}
	
	public static void update(){
		for (Object o : renderDb.getObjectList("Entities").getList()){
			ObjectEntry entity = (ObjectEntry) o;
			if (entity != null){
				
				if (((Entity) entity.getObject()).isEnabled()){
					Renderer.processEntity((Entity) entity.getObject());
				}
			}
		}
		
		if (renderTimer.tick()){
			render();
		}
		if (!window.isActive()){
			setActive(false);
		}
	}
	
	public static void kill(){
		window.kill();
	}
	
	public static void setTitle(String text){
		window.setTitle(text);
	}
	
	public static void render(){
		Renderer.render(renderDb.getObjectList("Lights"), activeCamera);
		window.update();
	}
	
	public static ObjectDatabase getDatabase(){
		return renderDb;
	}

	public static void addEntity(String string, Entity object) {
		object.setName(string);
		renderDb.addObject("Entities", string, object);
	}

	public static Model getModel(String string) {
		Model tmpModel = (Model) renderDb.getObject("Models", string);
		if (tmpModel != null){
			return tmpModel;
		}
		addModel(string);
		return (Model) renderDb.getObject("Models", string);
	}

	private static void addModel(String string) {
		Model tmpModel = Loader.loadOBJ(string);
		renderDb.addObject("Models", tmpModel.getName(), tmpModel);
	}

	public static Texture getTexture(String string) {
		Texture tmpTexture = (Texture) renderDb.getObject("Textures", string);
		if (tmpTexture != null){
			return tmpTexture;
		}
		addTexture(string);
		return (Texture) renderDb.getObject("Textures", string);
	}

	private static void addTexture(String string) {
		Texture tmpTexture = Loader.loadPNG(string);
		renderDb.addObject("Textures", tmpTexture.getName(), tmpTexture);
	}

	public static Matrix4f createViewMatrix(Camera camera){
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		
		Matrix4f.rotate((float) Math.toRadians(camera.getRotation().getX()), new Vector3f(1,0,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(camera.getRotation().getY()), new Vector3f(0,1,0), matrix, matrix);
		
		Vector3f negativeCameraPos = new Vector3f(-camera.getPosition().getX(),-camera.getPosition().getY(),-camera.getPosition().getZ());
		
		Matrix4f.translate(negativeCameraPos, matrix, matrix);
		return matrix;
	}

	public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f rotation, Vector3f scale) {
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.getX()), new Vector3f(1,0,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.getY()), new Vector3f(0,1,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotation.getZ()), new Vector3f(0,0,1), matrix, matrix);
		Matrix4f.scale(scale, matrix, matrix);
		return matrix;
	}
}
