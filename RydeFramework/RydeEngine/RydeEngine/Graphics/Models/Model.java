package RydeEngine.Graphics.Models;

public class Model {
	private int vaoId;

	private int vertexCount;
	
	private String name;
	
	private int heightMod = 0;
	
	public Model(int vaoId, int vertexCount){
		this.vaoId = vaoId;
		this.vertexCount = vertexCount;
	}
	
	public int getVaoId() {
		return vaoId;
	}

	public void setVaoId(int vaoId) {
		this.vaoId = vaoId;
	}

	public int getVertexCount() {
		return vertexCount;
	}

	public void setVertexCount(int vertexCount) {
		this.vertexCount = vertexCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHeightMod() {
		return heightMod;
	}

	public void setHeightMod(int heightMod) {
		this.heightMod = heightMod;
	}
}
