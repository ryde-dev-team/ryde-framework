package RydeEngine.Graphics.Renderers;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import RydeEngine.Graphics.Graphics;
import RydeEngine.Graphics.Entities.Camera;
import RydeEngine.Graphics.Entities.Entity;
import RydeEngine.Graphics.Entities.Light;
import RydeEngine.Graphics.Models.Model;
import RydeEngine.Graphics.Shaders.EntityShader;
import RydeEngine.Graphics.Textures.Texture;
import RydeEngine.System.ObjectDatabase;
import RydeEngine.System.ObjectEntry;
import RydeEngine.System.ObjectList;

public class EntityRenderer extends Renderer {
	private EntityShader shader;
	public EntityRenderer(Matrix4f projectionMatrix) {
		shader = new EntityShader(projectionMatrix);
	}
	
	public void render(ObjectDatabase entities, ObjectList lights, Camera camera) {
		shader.start();
		shader.loadSkyColor(RED, GREEN, BLUE);
		for (Object tmpLight : lights.getList()){
			shader.loadLight((Light)tmpLight);
		}
		
		shader.loadViewMatrix(camera);
		
				//Model Name
				//	Texture 1
				//		Entity 1
				//		Entity 3
				//	Texture 2
				//		Entity 2
				//		Entity 4
				//		Entity 5
				//	Texture 3
				//		Entity 9
				//Next Model
				//	Texture 1
				//		Entity 6
				//		Entity 8
				//	Texture 2
				//		Entity 7
				//		Entity 10
		
		
		while (entities.nextTable() != null){
			
			int modelVaoId = Integer.parseInt(entities.getCurrentTable().getName()); 
			prepareModel(modelVaoId);
			
			while (entities.getCurrentTable().nextEntry() != null){
				
				int textureGlId = Integer.parseInt(entities.getCurrentTable().getCurrentEntry()
						.getName());
				prepareTexture(textureGlId);
				
				ObjectList entity = (ObjectList) entities.getCurrentTable()
						.getCurrentEntry().getObject();
				
				for (Object o : entity.getList()){
					
					Entity instance = (Entity) o;
					prepareInstance(instance);
					GL11.glDrawElements(GL11.GL_TRIANGLES, instance.getModel()
							.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
				
				}
			}
			
			unbindModel();
		}
		
		shader.kill();
	}

	private void unbindModel() {
		Renderer.enableCulling();
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}

	private void prepareInstance(Entity entity) {
		shader.loadTransformationMatrix(entity);
		shader.loadOffset(entity.getTextureOffset());
	}

	private boolean prepareTexture(int t) {
		ObjectList textureList = Graphics.getDatabase().getObjectList("Textures");
		for (Object o: textureList.getList()){
			ObjectEntry oe = (ObjectEntry) o;
			Texture texture = (Texture) oe.getObject();
			if (texture.getGLId() == t){
				shader.loadNumberOfRows(texture.getNumberOfRows());
				if (texture.hasAlpha()){
					Renderer.disableCulling();
				}
				shader.loadFakeLightVariable(texture.hasFakeLight());
				shader.loadShineVariable(texture.getShineDamper(), texture.getReflectivity());
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getGLId());
			}
		}
		return false;
	}

	private boolean prepareModel(int m) {
		ObjectList modelList = Graphics.getDatabase().getObjectList("Models");
		for (Object o:modelList.getList()){
			ObjectEntry oe = (ObjectEntry) o;
			Model model = (Model) oe.getObject();
			if (model.getVaoId() == m){
				GL30.glBindVertexArray(m);
				GL20.glEnableVertexAttribArray(0);
				GL20.glEnableVertexAttribArray(1);
				GL20.glEnableVertexAttribArray(2);
				return true;
			}
		}
		return false;
	}

}
