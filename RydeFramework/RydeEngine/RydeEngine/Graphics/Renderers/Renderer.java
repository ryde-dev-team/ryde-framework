package RydeEngine.Graphics.Renderers;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;

import RydeEngine.Graphics.Graphics;
import RydeEngine.Graphics.Entities.Camera;
import RydeEngine.Graphics.Entities.Entity;
import RydeEngine.System.Console;
import RydeEngine.System.ObjectDatabase;
import RydeEngine.System.ObjectList;
import RydeEngine.System.ObjectTable;
import RydeEngine.System.Window;

public class Renderer {
	private static int width;
	private static int height;
	
	private static Matrix4f projectionMatrix;
	
	private static final float fov = 90;
	private static final float nearPlane = 0.1f;
	private static final float farPlane = 1000;
	
	protected static float RED = 1.0f;
	protected static float GREEN = 0.0f;
	protected static float BLUE = 0.0f;
	
	private static EntityRenderer entityRender;
	
	private static ObjectDatabase entities = new ObjectDatabase();
	
	public static void setDimensions(int w, int h) {
		width = w;
		height = h;
		GL11.glViewport(0, 0, width, height);
		createProjectionMatrix();
		loadRenderers();
	}
	
	public static void start(Window w){
		setDimensions(w.getWidth(),w.getHeight());
		enableCulling();
	}
	
	public static void enableCulling(){
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}
	
	public static void disableCulling(){
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	private static void createProjectionMatrix(){
		float aspectRatio = (float) width / (float) height;
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(fov / 2f))) * aspectRatio);
		float x_scale = y_scale / aspectRatio;
		float frustum_length = farPlane - nearPlane;
		
		projectionMatrix = new Matrix4f();
		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((farPlane + nearPlane) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * nearPlane * farPlane) / frustum_length);
		projectionMatrix.m33 = 0;
	}
	
	private static void loadRenderers(){
		entityRender = new EntityRenderer(projectionMatrix);
		//rendererList.add(new TerrainRenderer(projectionMatrix));
	}

	public static void processEntity(Entity entity) {
		String modelName = Integer.toString(entity.getModel().getVaoId());
		String textureName = Integer.toString(entity.getTexture().getGLId());
		
		
		if (entity == null || modelName == "" || textureName == ""){
			Console.warning("Can't process null entity");
			return;
		}
		
		ObjectTable modelTable = entities.getObjectTable(modelName);
		if (modelTable == null){
			entities.addTable(modelName);
		}
		
		ObjectList entityList = (ObjectList) entities.getObject(modelName, textureName);
		if (entityList == null){
			entities.addObject(modelName, textureName, new ObjectList());
			entityList = (ObjectList) entities.getObject(modelName, textureName);
		}
		
		entityList.add(entity);
		
		//Model Name
		//	Texture 1
		//		Entity 1
		//		Entity 3
		//	Texture 2
		//		Entity 2
		//		Entity 4
		//		Entity 5
		//	Texture 3
		//		Entity 9
		//Next Model
		//	Texture 1
		//		Entity 6
		//		Entity 8
		//	Texture 2
		//		Entity 7
		//		Entity 10
	
	}

	public static void render(ObjectList lightList, Camera camera) {
		prepare();
		entityRender.render(entities, lightList, camera);
		entities.print();
		entities.clear();
	}
	
	public static void prepare(){
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClearColor(RED, GREEN, BLUE, 0.0f);
		GL11.glClear( GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT );	
	}

}
