package RydeEngine.Graphics.Shaders;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import RydeEngine.Graphics.Graphics;
import RydeEngine.Graphics.Entities.Camera;
import RydeEngine.Graphics.Entities.Entity;
import RydeEngine.Graphics.Entities.Light;

public class EntityShader extends Shader{
	private static final String vertexPath = "RydeEngine/RydeEngine/Graphics/Shaders/entityVertex.shader";
	private static final String fragmentPath = "RydeEngine/RydeEngine/Graphics/Shaders/entityFragment.shader";

	//Uniform loacation variables
	private int location_projectionMatrix;
	private int location_transformationMatrix;
	private int location_viewMatrix;
	
	private int location_skyColor;
	
	private int location_lightPosition;
	private int location_lightColor;
	
	private int location_offset;
	private int location_numberOfRows;
	
	private int location_useFakeLight;
	
	private int location_shineDamper;
	private int location_reflectivity;
	
	public EntityShader(Matrix4f projectionMatrix) {
		super(vertexPath, fragmentPath);
		start();
		loadProjectionMatrix(projectionMatrix);
		kill();
	}

	@Override
	protected void bindAttributes() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void getAllUniformLocations() {
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
	}
	
	public void loadProjectionMatrix(Matrix4f projection){
		super.loadMatrix(location_projectionMatrix, projection);
	}

	public void loadSkyColor(float red, float green, float blue) {
		super.loadVector(location_skyColor, new Vector3f(red, green, blue));
	}

	public void loadLight(Light tmpLight) {
		super.loadVector(location_lightPosition, tmpLight.getPosition());
		super.loadVector(location_lightColor, tmpLight.getColor());
	}

	public void loadViewMatrix(Camera camera) {
		Matrix4f viewMatrix = Graphics.createViewMatrix(camera);
		super.loadMatrix(location_viewMatrix, viewMatrix);
	}

	public void loadTransformationMatrix(Entity entity) {
		Matrix4f transformationMatrix = Graphics.createTransformationMatrix(entity.getPosition(), entity.getRotation(), entity.getScale());
		super.loadMatrix(location_transformationMatrix, transformationMatrix);
	}

	public void loadOffset(Vector2f textureOffset) {
		super.loadVec2(location_offset, textureOffset);
	}

	public void loadNumberOfRows(int numberOfRows) {
		super.loadInt(location_numberOfRows, numberOfRows);
	}

	public void loadFakeLightVariable(boolean hasFakeLight) {
		super.loadBoolean(location_useFakeLight, hasFakeLight);
	}

	public void loadShineVariable(float shineDamper, float reflectivity) {
		super.loadFloat(location_shineDamper, shineDamper);
		super.loadFloat(location_reflectivity, reflectivity);
	}

}
