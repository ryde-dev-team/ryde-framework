package RydeEngine.Graphics.Shaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import RydeEngine.System.Console;

public abstract class Shader {
	private int id;
	private int vertexShaderId;
	private int fragmentShaderId;
	
	private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

	public Shader(String vertexpath, String fragmentpath) {
		vertexShaderId = loadShader(vertexpath, GL20.GL_VERTEX_SHADER);
		fragmentShaderId = loadShader(fragmentpath, GL20.GL_FRAGMENT_SHADER);
		
		id = loadProgram();
		
		getAllUniformLocations();
	}
	
	
	private int loadShader(String file, int type){
		StringBuilder shaderSource = new StringBuilder();
		
		try{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine()) != null){
				shaderSource.append(line).append("\n");
			}
			reader.close();
		} catch (IOException e) {
			Console.error("Could not reader shader " + file);
		}
		int shaderId = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderId,  shaderSource);
		GL20.glCompileShader(shaderId);
		if (GL20.glGetShaderi(shaderId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE){
			Console.error("Could not compile shader. Issue with source.");
		}
		return shaderId;
	}
	
	private int loadProgram(){
		int programId = GL20.glCreateProgram();
		GL20.glAttachShader(id, vertexShaderId);
		GL20.glAttachShader(id, fragmentShaderId);
		bindAttributes();
		GL20.glLinkProgram(id);
		GL20.glValidateProgram(id);
		return programId;
	}
	
	
	protected abstract void bindAttributes();
	
	protected void bindAttribute(int attribute, String variableName){
		GL20.glBindAttribLocation(id, attribute, variableName);
	}
	
	protected abstract void getAllUniformLocations();
	
	protected int getUniformLocation(String uniformName){
		return GL20.glGetUniformLocation(id, uniformName);
	}
	
	
	public void start(){
		GL20.glUseProgram(id);
	}
	
	public void kill(){
		GL20.glUseProgram(0);
	}
	
	public void cleanUp(){
		kill();

		GL20.glDetachShader(id, vertexShaderId);
		GL20.glDeleteShader(vertexShaderId);
		
		GL20.glDetachShader(id, fragmentShaderId);
		GL20.glDeleteShader(fragmentShaderId);
		
		GL20.glDeleteProgram(id);
	}
	
	
	protected void loadMatrix(int location, Matrix4f matrix){
		matrix.store(matrixBuffer);
		matrixBuffer.flip();
		GL20.glUniformMatrix4(location, false, matrixBuffer);
	}


	public void loadVector(int location, Vector3f vector) {
		GL20.glUniform3f(location,  vector.x, vector.y, vector.z);
	}


	public void loadVec2(int location, Vector2f vector) {
		GL20.glUniform2f(location,  vector.x, vector.y);
	}


	public void loadBoolean(int location, boolean value) {
		float toLoad = 0;
		if (value){
			toLoad = 1;
		}
		GL20.glUniform1f(location,  toLoad);
	}


	public void loadFloat(int location, float value) {
		GL20.glUniform1f(location,  value);
	}


	public void loadInt(int location, int value) {
		GL20.glUniform1i(location, value);
	}
}
