package RydeEngine.Graphics.Textures;

import org.lwjgl.util.vector.Vector2f;

public class Texture {
	private String name;
	private int glId;
	
	//Texture Maps
	private int numberOfRows = 1;
	private int index = 0;
	
	private boolean alpha = false;
	private boolean fakeLight = false;
	
	private float shineDamper = 10;
	private float reflectivity = 0.5f;
	
	public Texture(String name, int id){
		this.name = name;
		this.glId = id;
	}
	
	public Texture(String name, int id, int rows, int index){
		this.name = name;
		this.glId = id;
		this.numberOfRows = rows;
		this.index = index;
	}
	
	public void setAlpha(boolean state){
		alpha = state;
	}
	
	public void setFakeLight(boolean state){
		fakeLight = state;
	}
	
	public void setShineDamper(float value){
		shineDamper = value;
	}
	
	public void setReflectivity(float value){
		reflectivity = value;
	}

	public String getName() {
		return name;
	}

	public int getGLId() {
		return glId;
	}

	public Vector2f getOffset() {
		int column = index%numberOfRows;
		int row = index/numberOfRows;
		return new Vector2f((float)column/(float)numberOfRows,(float)row/(float)numberOfRows);
	}
	
	public void setIndex(int i){
		index = i;
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public boolean hasAlpha() {
		return alpha;
	}

	public boolean hasFakeLight() {
		return fakeLight;
	}

	public float getShineDamper() {
		return shineDamper;
	}
	
	public float getReflectivity(){
		return reflectivity;
	}
}
