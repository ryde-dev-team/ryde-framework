package RydeEngine.System;

public class Console {
	public static void debug(String text){
		System.out.println("[DEBUG] " + text);
	}
	public static void info(String text){
		System.out.println("[INFO] " + text);
	}
	public static void warning(String text){
		System.out.println("[WARNING] " + text);
	}
	public static void error(String text){
		System.out.println("[ERROR] " + text);
	}
}
