package RydeEngine.System.Loaders;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import RydeEngine.Graphics.Models.Model;
import RydeEngine.Graphics.Textures.Texture;

public class Loader {
	
	public static List<Integer> vaos = new ArrayList<Integer>();
	public static List<Integer> vbos = new ArrayList<Integer>();
	public List<Integer> textures = new ArrayList<Integer>();
	
	public static Model loadOBJ(String modelName){
		OBJData data = OBJLoader.loadOBJ(modelName);
		Model model = loadToVAO(data);
		model.setName(modelName);
		return model;
	}
	
	public static Model loadToVAO(OBJData data){ //
		int vaoID = createVAO();
		bindIndicesBuffer(data.getIndices());
		storeDataInAttributeList(0,3,data.getVertices());
		storeDataInAttributeList(1,2,data.getTextureCoords());
		storeDataInAttributeList(2,3,data.getNormals());
		unbindVAO();
		return new Model(vaoID, data.getIndices().length);
	}
	
	public static int createVAO(){
		int vaoID = GL30.glGenVertexArrays();
		vaos.add(vaoID);
		GL30.glBindVertexArray(vaoID);
		return vaoID;
	}
	
	private static void bindIndicesBuffer(int[] indices){
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	public static void storeDataInAttributeList(int attributeNumber, int coordinateSize, float[] data){
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0,0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER,0);
	}
	
	public static void unbindVAO(){
		GL30.glBindVertexArray(0);
	}
	
	private static IntBuffer storeDataInIntBuffer(int[] data){
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static FloatBuffer storeDataInFloatBuffer(float[] data){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	public static Texture loadPNG(String string) {
		Texture texture = PNGLoader.loadPNG(string);
		return texture;
	}
}
