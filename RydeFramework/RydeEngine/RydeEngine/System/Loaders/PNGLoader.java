package RydeEngine.System.Loaders;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.TextureLoader;

import RydeEngine.Graphics.Textures.Texture;
import RydeEngine.System.Console;

public class PNGLoader {

	public static Texture loadPNG(String string) {
		org.newdawn.slick.opengl.Texture texture = null;
		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("data/textures/" + string + ".png"));
			GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.4f);
		
		} catch (FileNotFoundException e) {
			Console.error("Could not find texture " + string + ".png");
		} catch (IOException e) {
			Console.error("Could not load texture " + string + ".png");
		}
		return new Texture(string,texture.getTextureID());
	}

}
