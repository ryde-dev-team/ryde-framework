package RydeEngine.System;

import RydeEngine.Graphics.Entities.Entity;

public class ObjectDatabase {
	private ObjectList tableList = new ObjectList();
	
	private int cursor = -1;
	
	public void addTable(String name){
		tableList.add(new ObjectTable(name));
	}
	
	public Object getObject(String tableName, String objectName){
		ObjectTable tmpTable = getObjectTable(tableName);
		return tmpTable.getObject(objectName);
	}
	
	public void addObject(String tableName, String objectName, Object object){
		ObjectTable tmpTable = getObjectTable(tableName);
		tmpTable.addObject(objectName, object);
	}
	
	public ObjectTable getObjectTable(String name){
		for (Object o : tableList.getList()){
			ObjectTable tmpTable = (ObjectTable) o;
			if (tmpTable.getName() == name){
				return tmpTable;
			}
		}
		return null;
	}
	
	public void print(){
		Console.info("========== Printing Database ==========");
		for (Object o : tableList.getList()){
			ObjectTable tmpTable = (ObjectTable) o;
			tmpTable.print();
		}
	}

	public ObjectList getObjectList(String string) {
		ObjectTable tmpTable = getObjectTable(string);
		return tmpTable.getList();
	}

	public ObjectTable nextTable() {
		cursor++;
		int index = 0;
		for (Object o : tableList.getList()){
			if (index == cursor){
				return (ObjectTable) o;
			}
			index++;
		}
		return null;
	}

	public ObjectTable getCurrentTable() {
		int index = 0;
		for (Object o : tableList.getList()){
			if (index == cursor){
				return (ObjectTable) o;
			}
			index++;
		}
		return null;
	}

	public void clear() {
		tableList = new ObjectList();
		cursor = -1;
	}
}
