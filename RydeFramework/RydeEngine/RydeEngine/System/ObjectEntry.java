package RydeEngine.System;

public class ObjectEntry {
	private String name;
	private Object object;

	public ObjectEntry(String n, Object o) {
		name = n;
		object = o;
	}

	public String getName() {
		return name;
	}

	public Object getObject() {
		return object;
	}

}
