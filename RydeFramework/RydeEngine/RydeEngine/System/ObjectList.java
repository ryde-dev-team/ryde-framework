package RydeEngine.System;

import java.util.ArrayList;
import java.util.List;

public class ObjectList {
	List<Object> list = new ArrayList<Object>();
	
	protected boolean locked = false;
	
	public List<Object> getList(){
		return list;
	}
	
	public int add(Object object){
		if (isLocked()){
			return -1;
		} else {
			lock();
		}
		
		if (isMember(object)){
			Console.warning("Duplicate object ignored.");
		}
		
		int id = nextFreeId();
		if (id > -1){
			list.add(id, object);
		} else {
			list.add(object);
			id = getId(object);
		}
		
		unlock();
		return id;
	}
	
	public Object get(int id){
		return list.get(id);
	}
	
	public int getId(Object object){
		return list.indexOf(object);
	}
	
	public void remove(Object object){
		list.set(getId(object), null);
	}
	
	public void remove(int id){
		list.set(id, null);
	}
	
	public void lock(){
		locked = true;
	}
	
	public void unlock(){
		locked = false;
	}
	
	public boolean isLocked(){
		return locked;
	}
	
	public boolean isMember(Object object){
		for (Object check : list){
			if (object == check){
				return true;
			}
		}
		return false;
	}
	
	public boolean isFree(int id){
		if (list.get(id) == null){
			return true;
		}
		return false;
	}
	
	public int nextFreeId(){
		int counter = 0;
		for (Object object : list){
			if (isFree(counter)){
				return counter;
			}
			counter++;
		}
		return -1;
	}
}
