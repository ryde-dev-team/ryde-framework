package RydeEngine.System;

import RydeEngine.Graphics.Entities.Entity;

public class ObjectTable {
	
	private String name;
	private ObjectList objects = new ObjectList();
	
	private int cursor = -1;
	
	public ObjectTable(String n) {
		name = n;
	}
	
	public void addObject(String name, Object o){
		objects.add(new ObjectEntry(name, o));
	}
	
	public String getName(){
		return name;
	}

	public Object getObject(String objectName) {
		for (Object object : objects.getList()){
			ObjectEntry oe = (ObjectEntry) object;
			if (oe.getName() == objectName){
				return oe.getObject();
			}
		}
		return null;
	}

	public void print() {
		Console.info("Table: " + name);
		for (Object object : objects.getList()){
			ObjectEntry oe = (ObjectEntry) object;
			Console.info("   Entry: " + oe.getName());
		}
	}
	
	public ObjectList getList(){
		return objects;
	}

	public ObjectEntry nextEntry() {
		cursor++;
		int index = 0;
		for (Object o : objects.getList()){
			if (index == cursor){
				return (ObjectEntry) o;
			}
			index++;
		}
		return null;
	}

	public ObjectEntry getCurrentEntry() {
		int index = 0;
		for (Object o : objects.getList()){
			if (index == cursor){
				return (ObjectEntry) o;
			}
			index++;
		}
		return null;
	}
}
