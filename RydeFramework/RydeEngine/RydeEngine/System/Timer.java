package RydeEngine.System;

public class Timer implements Runnable {
	private long maxTime;
	private long timeStarted;
	private long currentTime;
	
	private boolean active = false;
	
	private Thread thread = new Thread(this);
	
	private boolean paused = false;

	public Timer(int i) {
		maxTime = i;
		
	}
	
	public void start(){
		timeStarted = System.currentTimeMillis();
		active = true;
		thread.start();
	}

	@Override
	public void run() {
		while(active){
			currentTime = System.currentTimeMillis()-timeStarted;
			try {
				thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public boolean isDone(){
		if (currentTime >= maxTime){
			return true;
		}
		return false;
	}
	
	public void reset(){
		timeStarted = System.currentTimeMillis();
	}
	
	public boolean tick(){
		if (!isDone()){
			return false;
		}
		reset();
		return true;
	}
	
	public void print(){
		Console.info("Timer: " + currentTime + " / " + maxTime);
	}
}
