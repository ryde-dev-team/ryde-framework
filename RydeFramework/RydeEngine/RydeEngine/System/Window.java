package RydeEngine.System;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

public class Window {
	private int width;
	private int height;
	
	private int frameRate;
	private Timer frameRateTimer = new Timer(1000);
	private int currentFrameRate = 0;
	private int frames = 0;
	
	private String title;
	
	private boolean active = false;
	
	public Window(int w, int h, int fps) {
		width = w;
		height = h;
		frameRate = fps;
		
		setDimensions(w,h);
		createDisplay(24);
		setTitle("Untitled Window");
		
		frameRateTimer.start();
		setActive(true);
	}
	
	public void setDimensions(int w, int h){
		width = w;
		height = h;
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
		} catch (LWJGLException e) {
			Console.error("COuld not set display mode to " + width +"x" + height);
		}
	}
	
	public void createDisplay(int depthBits){
		ContextAttribs attribs = new ContextAttribs(3, 2)
				.withForwardCompatible(true)
				.withProfileCore(true);
		
		try {
			Display.create(new PixelFormat().withDepthBits(depthBits),attribs);
		} catch (LWJGLException e) {
			Console.error("Could not create display with a depth of " + depthBits);
		}
	}
	
	public void setTitle(String text){
		title = text;
		Display.setTitle(title);
	}
	
	public void update(){
		if (Display.isCloseRequested()){
			setActive(false);
		}
		
		Display.sync(frameRate);
		Display.update();
		if (frameRateTimer.tick()){
			currentFrameRate = frames;
			frames = 0;
		} else {
			frames++;
		}
		
	}
	
	public void setActive(boolean state){
		active = state;
	}
	
	public void kill(){
		Display.destroy();
	}
	
	public boolean isActive(){
		return active;
	}
	
	public int getCurrentFrameRate(){
		return currentFrameRate;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}

}
