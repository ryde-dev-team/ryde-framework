package RydeEngine.Tests;

import RydeEngine.Graphics.Graphics;
import RydeEngine.Graphics.Entities.Sphere;
import RydeEngine.System.Console;
import RydeEngine.System.Timer;

public class AppTest {

	public static void main(String[] args) {
		Console.info("Starting App Test");
		
		Graphics.start(800, 600, 60);
		Graphics.setTitle("AppTest");
		
		Graphics.addEntity("testSphere", new Sphere());
		Graphics.getDatabase().print();
		
		while (Graphics.isActive()){
			Graphics.update();
		}
		
		Console.info("Killing App Test");
		Graphics.kill();
		System.exit(0);
	}

}
